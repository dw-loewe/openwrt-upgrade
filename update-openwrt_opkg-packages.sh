#!/bin/ash

# script written by David Weber <mail@dw-loewe.de>

opkg update
# find out, if there are any upgradable packages
#UPGRADABLECNT=$(opkg list-upgradable | cut -f 1 -d ' ' | wc -l )
opkg list-upgradable | cut -f 1 -d ' ' > upgradable-packages.list
UPGRADABLECNT=`wc -l upgradable-packages.list | awk '{ print $1 }'`

if [ ${UPGRADABLECNT} -eq 0 ]; then
	echo "no updates; exiting"
	exit 1
else
	echo "available updates: ${UPGRADABLECNT}"
	cat upgradable-packages.list | xargs opkg upgrade
#	cat upgradable-packages.list | xargs opkg --download-only
#	cat upgradable-packages.list | xargs opkg upgrade --cache
fi

