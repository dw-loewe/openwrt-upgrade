#!/bin/bash
# Autor: David Weber <mail@dw-loewe.de> // 17.12.2020
# Lizenz: GPLv3 (https://www.gnu.org/licenses/gpl.txt)
#
# läd aktuelle OpenWRT-Version herunter, verifiziert diese und überträgt diese auf Router
#
# Voraussetzungen:
#
# - entsprechend Anleitung von Kuketz Blog installierter & konfigurierter openwrt-Router: https://www.kuketz-blog.de/stubby-verschluesselte-dns-anfragen-openwrt-teil5
#   - Stubby mit DNS over TLS (DoT)
# - Programme, die auf remote host vorhanden sein müssen:
#   - ssh
#   - wget
#   - gpg
#   - sha256sum
# - Skripte auf router hinterlegt:
#   - /root/bin/opkgscript.sh (https://github.com/richb-hanover/OpenWrtScripts/blob/master/opkgscript.sh)
#   - /root/bin/update-openwrt_opkg-packages.sh
#   - cron-skript, das auf letzteres zeigt, abrufbar mit crontab -l
# - ssh-Zugriff (root) auf router
# - GPG-Key im store installiert
# - internet-connect

RISK_BRICK=1				# risk to get a bricked router? (EXECUTE script, no security advices)

##################

ROUTER='router-trutzrollende'		# hostname

WANDEVNAME='wan'			# device name of ipv4 wan device
WAN6DEVNAME='wan6'			# device name of ipv6 wan device

# TP-Link Archer C7 AC1750 v2

ROUTERTARGET='ath79'			# routers board architecture
ROUTERVENDOR='generic'			# router vendor
ROUTERMODEL='tplink_archer-c7-v2'	# router model name

# MikroTik RouterBoard RB750r2 (hEX lite)

#ROUTERTARGET='ar71xx'			# routers board architecture
#ROUTERVENDOR='mikrotik'		# router vendor
#ROUTERMODEL='rb-nor-flash-16M'		# router model name

#TMPRESOLV='5.9.164.112'		# temporary resolver ipv4 (dns3.digitalcourage.de; https://digitalcourage.de/support/zensurfreier-dns-server)
#TMPRESOLV6='2a01:4f8:251:554::2'	# temporary resolver ipv6 (dns3.digitalcourage.de; https://digitalcourage.de/support/zensurfreier-dns-server)
TMPRESOLV='46.182.19.48'		# temporary resolver ipv4 (dns2.digitalcourage.de; https://digitalcourage.de/support/zensurfreier-dns-server)
TMPRESOLV6='2a02:2970:1002::18'	# temporary resolver ipv6 (dns2.digitalcourage.de; https://digitalcourage.de/support/zensurfreier-dns-server)

if [ -z $1 ]; then
	echo "Argument fehlt: $0 <aktuelle Version>
Beispiel:
	$0 21.02.1
die aktuelle Version ist auf der Startseite von https://openwrt.org zu finden ;)"
	exit 1
fi
OWRTVERS="$1"				# OpenWRT-Version, die installiert werden soll


# lege Arbeits-/Backupverzeichnisse fest; logging
SCRIPTPATH=$(readlink -f $0)

WORKDIR="$(dirname $SCRIPTPATH)/${ROUTER}_$(date +%Y-%m-%d_%H-%M-%S)_sysupgrade"


unset SCRIPTPATH
BKPDIR="$WORKDIR/backup"
LOGFILE=$WORKDIR/$(basename $0)
IDIR="$WORKDIR/images/$OWRTVERS"
[ -d $WORKDIR ] || mkdir -v $WORKDIR
[ -d $BKPDIR ] || mkdir -v $BKPDIR
echo "Arbeitsverzeichnis: $WORKDIR"

exec > >(tee "$LOGFILE.log" >&2) 2>&1

OWRTPREFIX="openwrt-${OWRTVERS}-${ROUTERTARGET}-${ROUTERVENDOR}-${ROUTERMODEL}"
IMAGE_KERNEL="${OWRTPREFIX}-initramfs-kernel.bin"
IMAGE_SYSUPGRADE="${OWRTPREFIX}-squashfs-sysupgrade.bin"
DLURL="http://downloads.openwrt.org/releases/${OWRTVERS}/targets/${ROUTERTARGET}/${ROUTERVENDOR}"

askforresume() {
	echo; echo
	echo "Fortfahren mit $1? (Bestätigung mit <ENTER>, Abbruch mit <Strg>+<c>)"
	echo; echo
	read
}
copyimagetorouter() {
	echo "kopiere image auf router"
	scp -4 ${IMAGE_SYSUPGRADE} ${ROUTER}:/tmp/ || exit 1
}
checkimageonrouter() {
	echo "überprüfe image (hash)"
	grep "${IMAGE_SYSUPGRADE}" sha256sums | sed 's! \*!  /tmp/!g' | ssh -4 $ROUTER sha256sum -c -
}

[ -d $IDIR ] || mkdir -p -v $IDIR

# lade openwrt images runter
echo "lade openwrt images runter"
for DLFILE in sha256sums.sig sha256sums.asc sha256sums "${IMAGE_KERNEL}" "${IMAGE_SYSUPGRADE}"; do
echo "${DLURL}/${DLFILE}"
wget -q -P $IDIR/ ${DLURL}/${DLFILE}
done

echo "bei downloads alles gut gegangen?"
askforresume "Erstellung Backup aller installierten Pakete"

# erstelle Backup aller installierten Pakete
echo "erstelle Backup aller installierten Pakete"
ssh -4 $ROUTER /root/bin/opkgscript.sh write || exit 1
scp -4 ${ROUTER}:/etc/config/.opkg.installed $BKPDIR/

# erstelle Backup vom System
echo "erstelle Backup vom System"
ssh -4 $ROUTER sysupgrade -b - | cat > $BKPDIR/sysbackup.tar.gz
ssh -4 $ROUTER crontab -l > $BKPDIR/crontab
scp -4 -r ${ROUTER}:/root/bin $BKPDIR/

cd $IDIR
echo "überprüfe Downloads (gpg & sha256sum)"
# überprüfe pgp-signierung der hashsummen-Datei
gpg --verify sha256sums.asc || exit 1

# überprüfe binaries anhand hashes
sha256sum --ignore-missing -c sha256sums || exit 1

# übertragen des images auf router & überprüfen
echo "übertragen des images auf router & überprüfen" 

# verhindere zu häufiges kopieren (SD-Verschleiss)
if ssh -4 $ROUTER test -f /tmp/${IMAGE_SYSUPGRADE}; then
	echo "image bereits auf $ROUTER vorhanden: /tmp/${IMAGE_SYSUPGRADE}"
	checkimageonrouter || { ssh -4 $ROUTER rm /tmp/${IMAGE_SYSUPGRADE}; copyimagetorouter; checkimageonrouter || exit 1 ; }
else
	copyimagetorouter; checkimageonrouter || exit 1
fi

cd $WORKDIR

askforresume "Anpassung der System-Konfiguration (für internetfähiges Standardsystem ohne Zusatzsoftware nach upgrade)"

# Voraussetzungen für Standardsystem (nach upgrade) schaffen
# https://www.kuketz-blog.de/openwrt-upgrade-einspielen-openwrt-teil7

[ -n $WAN6DEVNAME ] && ssh -4 $ROUTER /bin/ash <<EOF
uci set network.${WAN6DEVNAME}.dns='$TMPRESOLV6'
EOF

ssh -4 $ROUTER /bin/ash <<EOF
#	uci del dhcp.cfg01411c.noresolv
#	uci del dhcp.cfg01411c.server
	uci set dhcp.@dnsmasq[-1].noresolv=-1
	uci set dhcp.@dnsmasq[-1].server='127.0.0.1'
#	uci del_list dhcp.@dnsmasq[-1].server='127.0.0.1#5453'
#	uci del_list dhcp.@dnsmasq[-1].server='0::1#5453'
	uci set network.${WANDEVNAME}.dns='$TMPRESOLV'
	uci set dhcp.@dnsmasq[-1].dnsseccheckunsigned=-1
	uci set dhcp.@dnsmasq[-1].dnssec=-1
	uci -q commit && reload_config
	/etc/init.d/dnsmasq restart
EOF

# erstelle Backup vom System
askforresume "Erstellung eines Zwischenstand-Backups des Systems"
ssh -4 $ROUTER sysupgrade -b - | \
cat > $BKPDIR/sysbackup_post-stubby-disabled.tar.gz

# führe eigentliches sysupgrade aus
askforresume "Durchführung des eigentlichen Systemupgrades"

ssh -4 $ROUTER /bin/ash <<EOF
	sysupgrade -c /tmp/${IMAGE_SYSUPGRADE}
EOF


echo -n "Systemupgrade durchgeführt, warte auf reboot des Routers (kann einige Minuten dauern!)..."

sleep 20s
AVAIL=0
s=3
n=0
o=240
while [ $AVAIL -ne 1 -a $n -lt 150 ]; do
	sleep ${s}s
#	ping -c 2 $ROUTER > /dev/null
	ping -c 2 $ROUTER > /dev/null 2>&1
	[ $? -eq 0 ] && AVAIL=1
	echo -n .
	n=$(expr $n + 1)
done


if [ $AVAIL -ne 1 ]; then
	echo "beim Systemupgrade scheint ein gravierender Fehler aufgetreten zu sein, in dessen Folge der Router nicht mehr erreichbar ist. Beende Skript vorzeitig."
	exit 1
else
	echo "warte auf router ..."
	sleep 30s  # gebe dem router Zeit, ganz hochzufahren, bis auch ssh gestartet ist..
	echo "Teste ssh-verbindung..."
	ssh -4 $ROUTER /bin/ash <<EOF
		cat /etc/banner
EOF
	echo
	if [ $? -eq 0 ]; then
		echo "...Erfolg! Router ist wieder erreichbar!"
	else
		echo "SSH-Verbindung fehlgeschlagen; beende Skript vorzeitig."
		exit 1
	fi
fi


askforresume "Übertragung von Helferskripten und crontab-Einträgen und Installation von dnsmasq-full und stubby sowie weiterer vor dem upgrade vorhandener Pakete"

# übetrage (mit upgrade gelöschte) Helferskripte
ssh -4 $ROUTER "mkdir /root/bin"
ssh -4 $ROUTER "chmod 700 /root/bin"
scp -4 -r $BKPDIR/bin ${ROUTER}:/root/
scp -4 ${BKPDIR}/.opkg.installed ${ROUTER}:/etc/config/


# Paketlisten laden, dnsmasq-full & stubby installieren und (re)installiere (vor upgrade vorhandene) Pakete wieder

askforresume "Paketlisten laden, dnsmasq-full herunterladen"

ssh -4 $ROUTER /bin/ash <<EOF
	opkg update
	opkg install dnsmasq-full --download-only
EOF

askforresume "Installation von dnsmasq-full & stubby und (re)installieren weiterer (vor upgrade vorhandener) Pakete"

ssh -4 $ROUTER /bin/ash <<EOF
	opkg remove dnsmasq
	opkg install dnsmasq-full --cache .
	rm /root/*.ipk
	opkg install stubby
	/etc/init.d/dnsmasq restart
	/etc/init.d/uhttpd restart
EOF

# gebe dem router Zeit zum Neustarten der services
sleep 15s

askforresume "Reaktivierung der stubby/DNSSEC-Konfiguration"

# reaktiviere stubby wieder
# - dnsmasq-Integration
# - DNS-Anfragen nicht an ISP-DNS übermitteln
# - aktiviere DNSSEC-Validierung via dnsmasq


ssh -4 $ROUTER /bin/ash <<EOF
	uci set dhcp.@dnsmasq[-1].server='127.0.0.1#5453'
	uci add dhcp.@dnsmasq[-1].server='0::1#5453'
	uci set dhcp.@dnsmasq[-1].noresolv=1
	uci set network.${WANDEVNAME}.peerdns='0'
	uci set network.${WANDEVNAME}.dns='127.0.0.1'
	uci set dhcp.@dnsmasq[-1].dnssec=1
	uci set dhcp.@dnsmasq[-1].dnsseccheckunsigned=1
EOF


[ -n $WAN6DEVNAME ] && ssh -4 $ROUTER /bin/ash <<EOF
	uci set network.${WAN6DEVNAME}.peerdns='0'
	uci set network.${WAN6DEVNAME}.dns='0::1'
	uci add_list dhcp.@dnsmasq[-1].server='0::1#5453'
EOF

ssh -4 $ROUTER /bin/ash <<EOF
	uci commit && reload_config
	/etc/init.d/dnsmasq restart
	/etc/init.d/stubby restart
	/etc/init.d/dnsmasq enable
	/etc/init.d/stubby enable
EOF

# übertrage crontabs
cat $BKPDIR/crontab | ssh -4 $ROUTER crontab -

# gebe dem router Zeit zum Neustarten der services
echo "warte auf router ..."
sleep 10s

echo "aktualisiere Pakete"
ssh -4 $ROUTER /bin/ash <<EOF
	/root/bin/update-openwrt_opkg-packages.sh
	/root/bin/opkgscript.sh install
EOF

askforresume "letztmaligem Neustart des routers"
ssh -4 $ROUTER reboot

